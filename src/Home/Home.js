import React from "react";
import Header from "../Header/Header";
import { Link } from 'react-router-dom';
import "./Home.css";
import construction from "./construction.png";

class Home extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
                <Header />
                <div className="home-main">
                    <h1>Page under construction</h1>
                    <div className="home-image">
                        <img src={construction} alt="" />
                    </div>
                    <Link to="/">
                        <div className="success-button-container">
                            <button className="success-button">Sign Up</button>
                        </div>
                    </Link>
                </div>

            </>
        );
    }
}

export default Home;