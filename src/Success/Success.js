import React from "react";
import success from "./success.png";
import "./Success.css";
import { Link } from 'react-router-dom';
import Header from "../Header/Header";

class Success extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
                {
                    <Link to="/home" >
                        <Header />
                    </Link>
                }
                <div className="success-main">
                    <h1>Signup was successful!</h1>
                    <div className="success-image">
                        <img src={success} alt="" />
                    </div>
                    <Link to="/home">
                        <div className="success-button-container">
                            <button className="success-button">Go to Home</button>
                        </div>
                    </Link>
                </div>
            </>
        );
    }
}

export default Success;