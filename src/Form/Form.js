import React from "react";
import "./Form.css";
import validator from 'validator';
import hero from "./hero.png";
import { Link, Navigate } from "react-router-dom";
import Header from "../Header/Header";
import Success from "../Success/Success";

class Form extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            firstName: "",
            lastName: "",
            age: "",
            gender: "",
            role: "",
            email: "",
            password: "",
            repeatPassword: "",
            termsAndConditions: false,
            validEmail: false,
            validFirstName: false,
            validLastName: false,
            validAge: false,
            validPassword: false,
            validRepeatPassword: false,
            validGender: false,
            validRole: false,
            success: false,
        }

        this.passwordRules = {
            minLength: 8,
            minLowercase: 0,
            minUppercase: 0,
            minNumbers: 0,
            minSymbols: 0,
            returnScore: false,
        }

    }

    handleFirstNameChange = (event) => {
        this.setState({
            firstName: event.target.value,
            validFirstName: this.validateName(event.target.value)
        });
    }

    handleLastNameChange = (event) => {
        this.setState({
            lastName: event.target.value,
            validLastName: this.validateName(event.target.value)
        });
    }

    handleAgeChange = (event) => {
        this.setState({
            age: event.target.value,
            validAge: this.validateAge(event.target.value)
        });
    }

    handeGenderChange = (event) => {
        this.setState({
            gender: event.target.value,
            validGender: event.target.value !== ""
        });
    }

    handleRoleChange = (event) => {
        this.setState({
            role: event.target.value,
            validRole: event.target.value !== ""
        });
    }

    handleEmailChange = (event) => {
        this.setState({
            email: event.target.value,
            validEmail: validator.isEmail(event.target.value)
        });
    }

    handlePasswordChange = (event) => {
        /* what if user decides to fill up repeat password first, to solve this
        validRepeatPassword needs to be checked twice
        */
        this.setState({
            password: event.target.value,
            validPassword: validator.isStrongPassword(event.target.value, this.passwordRules),
            validRepeatPassword: validator.isStrongPassword(event.target.value, this.passwordRules) ?
                this.state.repeatPassword === event.target.value : false
        });
    }

    handleRepeatPasswordChange = (event) => {
        /* what if user decides to fill up repeat password first, to solve this
        validRepeatPassword needs to be checked twice
        */
        this.setState({
            repeatPassword: event.target.value,
            validRepeatPassword: validator.isStrongPassword(event.target.value, this.passwordRules) ?
                this.state.password === event.target.value : false
        });
    }

    handleTermsAndConditionsChange = (event) => {
        this.setState({
            termsAndConditions: event.target.checked,
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.setState({
            success: true
        })
        // TODO: why this doesnt work here but works in render
        // think cause setState is going to cause a rerender
        // <Navigate to="/success" replace={true} />
        console.log(event);
    }

    validateName(name) {
        if (name.trim().length === 0) {
            return false;
        }
        return (Array.from(name).every((character) => {
            return character.toLowerCase() >= 'a' && character.toLowerCase() <= 'z';
        }));
    }

    validateAge(age) {
        return age.indexOf(" ") === -1 && Number(age) >= 14 && Number(age) <= 60;
    }

    disableSubmitButton() {
        // return !true;
        return !(this.state.validFirstName && this.state.validLastName
            && this.state.validAge && this.state.validGender
            && this.state.validRole && this.state.validEmail
            && this.state.validPassword && this.state.validRepeatPassword
            && this.state.termsAndConditions
        );
    }
    render() {
        const genderList = ["Male", "Female", "Non-Binary", "Prefer not to say"];
        const roleList = ["Developer", "Senior Developer", "Lead Engineer", "CTO"];

        return (
            <>
                {
                    // TODO: why this worked here and not in handleSubmitForm function
                    // prob caus no render cause no set change
                    this.state.success ?
                        <Success />
                        :
                        <>
                            <Link to="/home" >
                                <Header />
                            </Link>

                            <div className="main-container">
                                <form onSubmit={this.handleSubmit}>

                                    <h1>Create new acccount</h1>
                                    <p className="signIn-message">Already have an acccount? <Link to="/home">Sign In</Link></p>

                                    <label htmlFor="firstName" className="icon">
                                        <input id="firstName"
                                            type="text"
                                            value={this.state.firstName}
                                            onChange={this.handleFirstNameChange}
                                            placeholder="First Name"
                                        />
                                        <i className="fa-solid fa-user"></i>
                                    </label>

                                    {
                                        this.state.validFirstName ?
                                            <p>Valid First Name <i className="fa-solid fa-check"></i></p>
                                            :
                                            <p>Please make sure your first name contains only alphabets with no space</p>
                                    }

                                    <label htmlFor="lastName" className="icon">
                                        <input id="lastName"
                                            type="text"
                                            value={this.state.lastName}
                                            onChange={this.handleLastNameChange}
                                            placeholder="Last Name"
                                        />
                                        <i className="fa-solid fa-user"></i>
                                    </label>
                                    {
                                        this.state.validLastName ?
                                            <p>Valid Last Name <i className="fa-solid fa-check"></i></p>
                                            :
                                            <p>Please make sure your last name contains only alphabets with no space</p>
                                    }

                                    <label htmlFor="age" className="icon">
                                        <input id="age"
                                            type="text"
                                            value={this.state.age}
                                            onChange={this.handleAgeChange}
                                            placeholder="Age"
                                        />
                                        <i className="fa-solid fa-message"></i>
                                    </label>
                                    {
                                        this.state.validAge ?
                                            <p>Valid Age <i className="fa-solid fa-check"></i></p>
                                            :
                                            <p>Please enter your age between 14 and 60</p>
                                    }

                                    <label htmlFor="gender">
                                        <select id="gender"
                                            onChange={this.handeGenderChange}
                                            placeholder="what"
                                        >
                                            <option value="" selected disabled hidden>Choose Gender</option>
                                            {
                                                genderList.map((gender) => {
                                                    return (
                                                        <option key={gender} value={gender}>{gender}</option>
                                                    );
                                                })
                                            }
                                        </select>
                                    </label>
                                    {
                                        this.state.validGender ?
                                            <p>Valid Gender <i className="fa-solid fa-check"></i></p>
                                            :
                                            <p>Please select gender from the options</p>
                                    }

                                    <label htmlFor="role">
                                        <select id="role"
                                            onChange={this.handleRoleChange}
                                        >
                                            <option value="" selected disabled hidden>Select Role</option>
                                            {
                                                roleList.map((role) => {
                                                    return (<option key={role} value={role}>{role}</option>);
                                                })
                                            }
                                        </select>
                                    </label>
                                    {
                                        this.state.validRole ?
                                            <p>Valid Role <i className="fa-solid fa-check"></i></p>
                                            :
                                            <p>Please select role from the options</p>
                                    }

                                    <label htmlFor="email" className="icon">
                                        <input type="text"
                                            value={this.state.email}
                                            onChange={this.handleEmailChange}
                                            placeholder="Email"
                                        />
                                        <i className="fa-solid fa-envelope"></i>
                                    </label>
                                    {
                                        this.state.validEmail ?
                                            <p>Valid Email <i className="fa-solid fa-check"></i></p>
                                            :
                                            <p>Please enter your email e.g. yourName@example.com"</p>
                                    }
                                    <label htmlFor="password" className="icon">
                                        <input type="password"
                                            value={this.state.password}
                                            onChange={this.handlePasswordChange}
                                            placeholder="Password"
                                        />
                                        <i className="fa-solid fa-lock"></i>
                                    </label>
                                    {
                                        this.state.validPassword ?
                                            <p>Valid Password <i className="fa-solid fa-check"></i></p>
                                            :
                                            <p>Please enter your password with atleast 8 characters</p>
                                    }

                                    <label htmlFor="repeatPassword" className="icon">
                                        <input type="password"
                                            value={this.state.repeatPassword}
                                            onChange={this.handleRepeatPasswordChange}
                                            placeholder="Repeat Password"
                                        />
                                        <i className="fa-solid fa-lock"></i>
                                    </label>
                                    {
                                        this.state.validRepeatPassword ?
                                            <p>Valid Repeat Password <i className="fa-solid fa-check"></i></p>
                                            :
                                            <p>Please make sure both passwords match and have atleast 8 characters</p>
                                    }
                                    <label htmlFor="termsAndConditions">

                                        <input type="checkbox"
                                            id="termsAndConditions"
                                            value={this.state.termsAndConditions}
                                            onChange={this.handleTermsAndConditionsChange}
                                        /> <span>Please read our <a href="https://dribbble.com/terms" target="_blank" id="termsAndCoLink">Terms and Conditions</a></span>
                                    </label>
                                    {
                                        this.state.termsAndConditions ?
                                            <span className="doMarginBottom">You have accepted our terms and conditions <i className="fa-solid fa-check"></i></span>
                                            :
                                            <span className="doMarginBottom">Please accept the terms and conditions</span>
                                    }
                                    {
                                        this.disableSubmitButton() ?
                                            <span className="submit-message">Please fill the sign-up form above correctly</span>
                                            :
                                            <span className="submit-message">Click on the button below to submit the form</span>
                                    }
                                    <button type="submit"
                                        disabled={this.disableSubmitButton()}
                                    >
                                        Sign in
                                    </button>
                                </form>
                                <div className="hero-container">
                                    <img src={hero} alt="background-image" />
                                </div>
                            </div >
                        </>
                }
            </>
        );
    }
}

export default Form;