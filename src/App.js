import React from "react";
import Form from "./Form/Form";
import Header from "./Header/Header";
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Success from "./Success/Success";
import Home from "./Home/Home";

class App extends React.Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Form />} />
            {/* <Route path="/success" element={<Success />} /> */}
            <Route path="/home" element={<Home />} />
          </Routes>
        </BrowserRouter>
      </div>
    );
  };
}

export default App;
